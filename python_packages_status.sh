#!/usr/bin/env bash

set -euo pipefail

has_wheel()
{
    local platform=$1
    local package=$2
    python -m pip download --no-cache-dir --no-dependencies \
        --only-binary :all: --platform "$platform" $package >& /dev/null
}

install()
{
    local package=$1
    local out_file=$2
    python -m pip install --no-cache-dir --no-dependencies $package >& $out_file
}

NONE_TOTAL=0
ARCH_SPECIFIC_WITH_WHEEL=0
ARCH_SPECIFIC_WITHOUT_WHEEL_INSTALLABLE=0
ARCH_SPECIFIC_WITHOUT_WHEEL_NOT_INSTALLABLE=0

one_package()
{
    local package=$1
    echo -n "package $package | https://pypi.org/project/$package | "

    if has_wheel none $package; then
        echo "is_none | has_wheel"
        ((NONE_TOTAL+=1))
        return
    fi

    echo -n "is_arch_specific | "

    if has_wheel win_arm64 $package; then
        echo "has_wheel"
        ((ARCH_SPECIFIC_WITH_WHEEL+=1))
        return
    fi

    echo -n "no_wheel | "

    if has_wheel win_amd64 $package; then
        echo -n "wheel_amd64 | "
    fi

    if install $package install.log; then
        echo "is_installable"
        ((ARCH_SPECIFIC_WITHOUT_WHEEL_INSTALLABLE+=1))
        return
    fi

    echo "not_installable"
    ((ARCH_SPECIFIC_WITHOUT_WHEEL_NOT_INSTALLABLE+=1))
    # show latest lines of install
    tail -n 100 install.log
}

all_packages()
{
    local idx=1
    local num_packages=$#
    for package in "$@";
    do
        echo -n "$idx/$num_packages | "
        one_package "$package"
        ((idx+=1))
    done
}

setup_python()
{
    local version=$1
    echo -n "download python - "
    mkdir python
    pushd python >& /dev/null
    wget -q https://www.nuget.org/api/v2/package/pythonarm64/$version -O p.zip
    unzip p.zip > /dev/null
    export PATH=$(pwd)/tools/:$PATH
    popd >& /dev/null
    python --version
    python -m pip install --upgrade pip >& /dev/null
}

show_stats()
{
    num_packages=$#
    echo "------------------"
    show_number()
    {
        local description=$1
        local quantity=$2
        local percentage="$((quantity*100/num_packages))"
        echo "$description: $quantity ($percentage%)"
    }
    show_number "no arch packages      " $NONE_TOTAL
    show_number "with arm64 wheel      " $ARCH_SPECIFIC_WITH_WHEEL
    show_number "installable (no wheel)" $ARCH_SPECIFIC_WITHOUT_WHEEL_INSTALLABLE
    show_number "not installable       " $ARCH_SPECIFIC_WITHOUT_WHEEL_NOT_INSTALLABLE
}

main()
{
    if [ $# -lt 1 ]; then
        echo "usage: python_packages..."
        exit 1
    fi

    local tmp=tmp
    rm -rf $tmp
    mkdir $tmp
    cd $tmp
    trap "exit 1" SIGINT

    setup_python 3.10.5
    all_packages "$@"
    show_stats "$@"
}

main "$@"
