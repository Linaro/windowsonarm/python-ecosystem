#!/usr/bin/env bash

set -euo pipefail

# returns 5000 most downloaded packages on Pypi (reverse sorted by number of downloads)
# https://hugovk.github.io/top-pypi-packages/
url=https://hugovk.github.io/top-pypi-packages/top-pypi-packages-30-days.min.json
wget -q $url -O - |
   tr ',' '\n' |
   grep project | cut -f 4 -d '"'
