To check if the 500 top python packages can be installed for Windows on Arm:

```
bash -c './python_packages_status.sh $(./top_pypi_packages.sh | head -n 500)'
```
